FROM node:latest
RUN  mkdir /emp
WORKDIR /emp
COPY    package.json /emp
COPY    . /emp
RUN     npm install
CMD     ["npm","start"]
